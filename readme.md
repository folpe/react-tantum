# React Tantum

A component to display an amount with currency

## Installation

```js
npm install @folpe/react-tantum
```

or

```js
yarn add @folpe/react-tantum
```

## Example

```js
import React from 'react'
import Tantum from '@folpe/react-tantum'

const myComponent = () => {
  ;<Tantum>30525,50</Tantum>
}
export default myComponent
```

## Options (WIP)

| Props             |  Value  | Def                                                         |
| ----------------- | :-----: | :---------------------------------------------------------- |
| symbol            | String  | The currency symbol (default : €)                           |
| symbolBefore      | Boolean | If the currency must display before amount (default: false) |
| thousandSeparator | String  | The thousand separator (default: ' ')                       |
| decimalSeparator  | String  | The decimal separator (default: ',')                        |
| numberDecimal     | Integer | The number after float (default: 2)                         |
| vat               | String  | The vat name                                                |

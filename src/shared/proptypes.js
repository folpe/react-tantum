import {
  and,
  between,
  integer,
  keysOf,
  nonNegativeInteger,
} from 'airbnb-prop-types'
import {
  any,
  arrayOf,
  bool,
  func,
  node,
  number,
  object,
  oneOf,
  oneOfType,
  shape,
  string,
} from 'prop-types'

export {
  and,
  between,
  integer,
  any,
  arrayOf,
  bool,
  func,
  keysOf,
  node,
  number,
  object,
  oneOf,
  nonNegativeInteger,
  oneOfType,
  shape,
  string,
}

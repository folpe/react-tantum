import React from 'react'

import {
  number,
  oneOfType,
  string,
  bool,
  nonNegativeInteger,
} from './shared/proptypes'

const Tantum = ({
  children = 0,
  symbol = '€',
  symbolBefore = false,
  thousandSeparator = String.fromCharCode(160),
  decimalSeparator = ',',
  numberDecimal = 2,
  vat = null,
}) => {
  numberDecimal = Math.abs(parseInt(numberDecimal))
  let number = parseFloat(children.toString().replace(/,/g, '.'))
  const negative = number < 0 ? '-' : ''

  let i =
    parseInt((number = Math.abs(+number || 0).toFixed(numberDecimal)), 10) + ''
  let j
  j = (j = i.length) > 3 ? j % 3 : 0
  const priceNumber =
    (j ? i.substr(0, j) + thousandSeparator : '') +
    i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousandSeparator) +
    (numberDecimal
      ? decimalSeparator +
        Math.abs(number - i)
          .toFixed(numberDecimal)
          .slice(2)
      : '')
  return (
    <span>
      {symbolBefore && <span>{symbol}&nbsp;</span>}
      {negative && <span>{negative}</span>}
      <span>{priceNumber}</span>
      {!symbolBefore && <span>&nbsp;{symbol}</span>}
      {vat && <span>&nbsp;{vat}</span>}
    </span>
  )
}

Tantum.propTypes = {
  children: oneOfType([number.isRequired, string.isRequired]),
  symbol: string,
  symbolBefore: bool,
  thousandSeparator: string,
  decimalSeparator: string,
  numberDecimal: nonNegativeInteger,
  vat: string,
}
export default Tantum

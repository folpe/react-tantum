module.exports = {
  extends: [
    'standard',
    'prettier',
    'plugin:react/recommended',
    'plugin:jsx-a11y/recommended',
    'plugin:import/errors',
  ],
  plugins: ['prettier', 'react', 'jsx-a11y', 'import'],
  parser: 'babel-eslint',
  rules: {
    'prettier/prettier': [
      'error',
      {
        arrowParens: 'always',
        jsxSingleQuote: true,
        semi: false,
        singleQuote: true,
        trailingComma: 'es5',
      },
    ],
    'no-irregular-whitespace': 0,
  },
  settings: {
    react: {
      version: '16.8.6',
    },
  },
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    jest: true,
    node: true,
  },
}
